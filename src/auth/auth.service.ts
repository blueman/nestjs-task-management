import { Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { UserRepository } from "./user.repository";
import { InjectRepository } from "@nestjs/typeorm";
import { AuthCredentialsDto } from "./dto/auth-credentials.dto";
import { JwtService } from "@nestjs/jwt";
import { JwtPayload } from "./jwt-payload.interface";

@Injectable()
export class AuthService {

    private logger = new Logger('AuthService');
    
    constructor(
        @InjectRepository(UserRepository)
        private userRepository: UserRepository,
        private jwtService: JwtService
    ) {
    }

    async signUp(authCredentialDto: AuthCredentialsDto): Promise<{accessToken:string}> {
        this.userRepository.signUp(authCredentialDto);

        return this.getAccessToken(authCredentialDto.username);
    }

    async signIn(authCredentialDto: AuthCredentialsDto): Promise<{accessToken:string}> {
        const username = await this.userRepository.validateUserPassword(authCredentialDto);

        if (!username) {
            throw new UnauthorizedException('Invalid credentials');
        }

        return this.getAccessToken(username);
    }
    
    async getAccessToken(username: string): Promise<{accessToken:string}> {
        const payload: JwtPayload = { username };
        const accessToken = await this.jwtService.sign(payload);
        this.logger.debug(`Generated JWT Token with payload ${JSON.stringify(payload)}`);

        return { accessToken };
    }
}
