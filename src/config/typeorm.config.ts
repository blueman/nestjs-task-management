import { TypeOrmModuleOptions } from "@nestjs/typeorm";
require('dotenv').config();

export const typeOrmConfig: TypeOrmModuleOptions = {
    type: 'mysql',
    host: process.env.DB_HOST || 'localhost',
    port: parseInt(process.env.DB_PORT) || 3306,
    username: process.env.DB_USER || 'root',
    password: process.env.DB_PASS || 'root',
    database: process.env.DB_DATABASE || 'taskmanagment',
    entities: [__dirname + '/../**/*.entity.{js,ts}'],
    synchronize: true,
};