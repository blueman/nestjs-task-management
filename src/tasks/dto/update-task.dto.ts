import { TaskStatus } from "../task-status.enum";

export class UpdateTaskDto {
    title: string = 'default';
    description: string = '';
    status: TaskStatus = TaskStatus.OPEN;
}